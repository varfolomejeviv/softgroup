#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-02 16:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: functions.php:29
msgid "Primary"
msgstr ""

#: functions.php:40
msgid "Left sidebar"
msgstr ""

#. Name of the template
msgid "Home"
msgstr ""

#. Name of the theme
msgid "Soft Group"
msgstr ""

#. Description of the theme
msgid "test theme"
msgstr ""

#. URI of the theme
#. Author URI of the theme
msgid "#"
msgstr ""

#. Author of the theme
msgid "varfolomejev"
msgstr ""
