msgid ""
msgstr ""
"Project-Id-Version: Soft Group\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-02 16:57+0000\n"
"PO-Revision-Date: 2017-03-02 16:58+0000\n"
"Last-Translator: varfolomejev <varfolomejevi@gmail.com>\n"
"Language-Team: Ukrainian\n"
"Language: uk\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && "
"n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: functions.php:40
msgid "Left sidebar"
msgstr "Лівий сайдбар"

#. Name of the template
msgid "Home"
msgstr ""

#. Name of the theme
msgid "Soft Group"
msgstr ""

#. Description of the theme
msgid "test theme"
msgstr ""

#. URI of the theme
#. Author URI of the theme
msgid "#"
msgstr ""

#. Author of the theme
msgid "varfolomejev"
msgstr ""

#: functions.php:29
msgid "Primary"
msgstr "Головне"
